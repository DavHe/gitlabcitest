#!/bin/bash

DockerHubUrl="localhost:5000"
ProjectName=$1
Tag=$2
ImageName=""
Env=""
StackNname="$1_stack"

if [ -z "$1" ]; then
    echo "ProjectName NOT ALLOW empty"
    exit
fi	

if [ -z "$2" ]; then
    echo "Tag NOT ALLOW empty"
    exit
fi

ImageName="$DockerHubUrl/${ProjectName}:${Tag}"
echo "ImageName=$ImageName"
echo "ProjectName=$ProjectName"

# 產生 .env file給 docker.compose.yml使用
echo "ImageName=$ImageName" > .env # override
echo "ProjectName=$ProjectName" >> .env # append

# execute as a subcommand in order to avoid the variables remain set
(
	# export variables excluding comments
	[ -f .env ] && export $(sed '/^#/d' .env)
	
	docker image pull "$ImageName"	
	
	find=$(docker stack ls | grep $StackNname |wc -l)
	if [[ $find < 1 ]]; then
		docker stack deploy -c docker-compose.yml $StackNname
	else
		docker service update --image "$ImageName" --force "${StackNname}_my_service"
	fi
)
