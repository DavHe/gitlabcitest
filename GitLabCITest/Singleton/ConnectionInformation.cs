﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GitLabCITest.Singleton
{
    public class ConnectInformation
    {
        private static readonly object syncLock = new object(); 
        private static ConnectInformation conn;

        private string ConnectionStr { get; set; }

        public ConnectInformation()
        {
            var config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            ConnectionStr = config.GetConnectionString("DefaultConnection");
        }

        public static ConnectInformation GetInstance()
        {
            if (conn == null)
                lock (syncLock)
                {
                    if (conn == null)
                        conn = new ConnectInformation();
                }

            return conn;
        }

        public string GetConnectionString()
        {
            return ConnectionStr;
        }

        
    }
}
