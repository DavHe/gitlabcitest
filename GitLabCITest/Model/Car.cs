﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GitLabCITest.Model
{
    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set;  }
        public string Country { get; set; }
        public int Price { get; set; }
    }
}
