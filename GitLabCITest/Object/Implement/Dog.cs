﻿using GitLabCITest.Object.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GitLabCITest.Object.Implement
{
    public class Dog : IAnimal
    {
        public string Name { get ; set; }

        public Dog()
        {
            Name = "Dog";
        }

        public override string ToString()
        {
            return $"Dog - Name:{Name}";
        }
    }
}
