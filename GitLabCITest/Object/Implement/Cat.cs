﻿using GitLabCITest.Object.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GitLabCITest.Object.Implement
{
    public class Cat : IAnimal
    {
        public string Name { get ; set ; }

        public Cat()
        {
            Name = "Cat";
        }

        public override string ToString()
        {
            return $"Cat - Name:{Name}";
        }
    }
}
