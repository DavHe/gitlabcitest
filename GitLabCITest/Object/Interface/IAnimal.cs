﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GitLabCITest.Object.Interface
{
    public interface IAnimal
    {
        string Name { get; set; }

        string ToString();
    }
}
