﻿using GitLabCITest.Model;
using GitLabCITest.Object.Implement;
using GitLabCITest.Object.Interface;
using GitLabCITest.Repository.Implement;
using GitLabCITest.Singleton;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLabCITest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : ControllerBase
    {
        private readonly IEnumerable<IAnimal> animals;

        public TestController(IEnumerable<IAnimal> _animals)
        {
            animals= _animals;
        }

        [HttpGet]
        public string GetAnimal()
        {
            StringBuilder sb = new StringBuilder();
            foreach (IAnimal animal in animals)
            {
                sb.Append(animal.ToString());
                sb.Append("\r\n");
            }
            return sb.ToString() + "...";
        }

        [HttpGet]
        [Route("[action]")]
        public string GetTest()
        {
            string str = ConnectInformation.GetInstance().GetConnectionString();
            return str;
        }

        [HttpGet]
        [Route("[action]")]
        public IEnumerable<Car> GetCars()
        {
            CarRepository carRepository = new CarRepository(ConnectInformation.GetInstance().GetConnectionString());
            string[] countrys = new string[] { "USA", "Germany", "Taiwan"};
            return carRepository.GetCarsByCountry(countrys);
        }
    }
}
