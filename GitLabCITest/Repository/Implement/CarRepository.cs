﻿using Dapper;
using GitLabCITest.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GitLabCITest.Repository.Implement
{
    public class CarRepository
    {
        private readonly string connectStr;

        public CarRepository(string connStr)
        {
            connectStr = connStr;
        }

        public IEnumerable<Car> GetAllCars()
        {
            IEnumerable<Car> cars ;
            using (SqlConnection conn = new SqlConnection(connectStr))
            {
                string sql = "select * from Car ";
                cars = conn.Query<Car>(sql);
            }
                
            return cars;
        }

        public IEnumerable<Car> GetCarsByCountry(string[] country)
        {
            IEnumerable<Car> cars;
            using (SqlConnection conn = new SqlConnection(connectStr))
            {
                string sql = "select * from Car where country in @country";
                cars = conn.Query<Car>(sql, new { country = country});
            }

            return cars;
        }
    }
}
