#
# Stage: build
#
FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
# FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

WORKDIR /src
COPY . .

RUN dotnet restore
RUN dotnet publish -c Release -o publish GitLabCITest

#
# Stage: runtime
#
FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS runtime
# FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS runtime

EXPOSE	80/tcp
#EXPOSE	5001/tcp

WORKDIR /app
COPY --from=build /src/publish/ .

# Change timezone to local time
ENV TZ=America/Guyana
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# ENTRYPOINT ["dotnet", "ONEbook.StreamingService.dll"]
CMD ["dotnet", "GitLabCITest.dll"]
