DockerHubUrl="localhost:5000"
ProjectName=$1
ImageName=""
Tag=$2

if [ -z "$1" ]; then
    echo "ProjectName NOT ALLOW empty"
    exit
fi	

if [ -z "$2" ]; then
    echo "Tag NOT ALLOW empty"
    exit
fi

ImageName="$DockerHubUrl/${ProjectName}:${Tag}"

# 輸出到 runner 
echo "ImageName=$ImageName"

# 移除build cache
docker builder prune -f
# 移除沒tag的image
docker image prune -f

docker build -t "$ImageName" . &&
docker image push "$ImageName"
